The latest iteration can be viewed with:

```shell
reveal-md 2024-nedcamp-when-not-a-module-make-and-maintain.md
```

Press S to open the speaker view to see the notes.

# When There's Not a Module for That

SEE updated 'When There's Not a Module for That: How to Make (and Maintain) a New Module' in raw notes



You've built sites with Drupal 8 and know that with a few dozen modules (and a ton of configuring), you can do nearly everything in modern Drupal.

But what do you do when there's not a module for that?  Or the ones that exist don't quite meet your needs?

You make your own.

This session will help you take that step.  All you need to do is write two text files. The first file tells Drupal about the module; it’s not code. The second file can have as little as three lines of code in it.  Making a module is something that anyone can do. There are many (mostly simple) rules to follow and tons of tools to use—and lots of exploration to do. *Every* person developing a module is still learning.


## Learning Objectives

In this hands-on session, you will:

  * Learn how to decide when to make your own module.
  * Write a module that plays well with Drupal core and other modules.
  * Discover ways to explore the options for extending and overriding functionality provided by Drupal core and other modules.


## Target Audience

A person who has done some site building and has run into limitations or simply wants to try out developing with code.


## Prerequisites

The only prerequsite is having done some site building with Drupal, and so having familiarity with Drupal configuration and its limits.  Information gained will be equally relevant to Drupal 8 and Drupal 9.


### Speakers

#### Benjamin Melançon

Worker-owner, developer @ [Agaric](https://agaric.com/)

At [Agaric](http://agaric.com/), i use open source free software to give people and groups power over their online communication and web presence. To help all people gain power over our own lives, which we need to make progress toward justice and liberty, I volunteer at a nonprofit organization called, and for, [People Who Give a Damn](http://pwgd.org/).


#### Mauricio Dinarte

Drupal Developer @ [Agaric](http://agaric.com/)


**Track:**

  * Back-End
  * Site Building


As posted: https://www.midcamp.org/2020/topic-proposal/when-theres-not-module



Draft abstract:

With about fifty modules, you can do nearly everything in Drupal 8 (plus configuration, which isn't necessarily trivial).

But what do you do when there's not a module for that?

You make your own.

In this session, we will go over several scenarios and how to 



You've built sites with Drupal and know it is a powerful and modular system and that much of Drupal’s power is in its modules, dynamos of drop-in functionality that build on Drupal’s base system and on one another to do wonderful things.

What do you do when there isn't "a module for that", though?  You can write a module yourself.






Micky works with technical activists to connect people with the information and tools they need to move from being a global network to being a global movement based on solidarity.  She wrote about her experience in the Drupal community as a contributing author in "Ours to Hack and to Own."  She frequently speaks and presents, including recently:

    Platform Cooperativism 2019 - Who Owns the World? - New School, NYC
    BADcamp 2019 - San Francisco
    Keynote - LibrePlanet 2019


Recording: https://events.drupal.org/seattle2019/sessions/scaling-community-decision-making



David Valdez

Spoke at PHP meetup in Mexico City, assisted migration training at DrupalCon Seattle and other venues.


***

Planned presentation outline:


Part 1: Looking for modules - is there already a module for that?   10 minutes
Part 2: Contributing to an existing module - is there *almost* a module for that?  15 minutes
Part 3: Starting a new module - 25 minutes
           - Considerations for writing a new module
           - What tools are available?


Part 2: Contributing to an existing

* Add a feature, a "Mail" option to Social Simple: https://www.drupal.org/project/social_simple/issues/2899517
* Fix bugs, example from David, https://www.drupal.org/project/comment_notify or https://www.drupal.org/project/tzfield/issues/2904016
* Upgrade to Drupal 8, example from David https://www.drupal.org/project/flatcomments

Part 3:
* A "one line" module (alter hook)
* A plugin module ("Drupal 8 hooks")
* A module that was generated with Drupal Console https://www.drupal.org/project/ckeditor_youtube/ - simple and used by many, and slowly got more complex
* A module bringing in a PHP library and getting crazy (started with simple field formatter, can use this or another module to demo that) - https://drupal.org/project/inotherwords



Potential further discussions


TODO:
    
    * Find one line and simple plugin examples (David looking at)
    * Draw useful boilerplate talking points from DGD7 (Ben looking at)


## Predecessor for Drupal 7

(had not found yet when gave it again for 8 the first time, drat!)

https://prezi.com/g1ibga4sgmib/when-theres-not-a-module-for-that/

## Running this presentation

To view the presentation in all its glory (it is indeed more exciting than the `content/_index.md` markdown file!) type this command:

`hugo server`

And click the localhost link provided in the output.
