
Definitively slay the mythology that module-making is a mystic domain of Drupal druids.



https://api.drupal.org/api/drupal/core%21core.api.php/group/extending/


https://api.drupal.org/api/drupal/core!core.api.php/group/hooks/



Use Git: don't be afraid to change things.


Use a debugger.



### Hooks work by naming convention.

To implement a hook, take the 'hook' part off the hook name and replace it with your module's (machine) name.


### When to Contribute?

 When your module fills a need larger than your precise use case.

Note:  If you just want to build a module, finding a module idea that one or more people have been asking for is a good way to meet a need and avoid duplication.

See also http://groups.drupal.org/
contributed-module-ideas


### Finding a Drupal Function that Does What You Need


* Identify a page that produces output like what you want to see.
* Look up the page callback function for that page's menu item.
* See what functions are used (or database queries made) in the page callback function.

`grep -nHR --include=*.module 'admin/appearance' modules`


### Error messages mean progress!

`Parse error: syntax error, unexpected ';', expecting ')'`

You are having an effect on the world.  Or at least your local site.


## Getting help

The Internet.

It's good.


### Tip: Looking up Hooks

We can look up hook definitions and their function signatures the same way we look up Drupal functions, at api.drupal.org:

api.drupal.org/hook_help

 api.drupal.org
 drupalcontrib.org


### Examples project

[drupal.org/project/examples](https://www.drupal.org/project/examples)


Tip: grep

grep -nHR module_implements_alter .

./includes/module.inc:646:    if ($hook != 'module_implements_alter') {

./modules/system/system.api.php:1775:function hook_module_implements_alter(&$implementations, $hook) {


### Tip: Clear Caches

If something you do has no effect...

`drush cr`


## Coding Standards


drupal.org/coding-standards

Automated Nagging

drupal.org/project/coder

Automatic Adherence

drupal.org/project/grammar_parser
(obsolete?)






## Development Tips

* When Something Isn't Working, Clear Caches
* When Anything's Missing, Check Permissions
* Set Your Site to Show All Errors

Lines to add to settings.php to show all notices and errors:

```
error_reporting(-1);
$conf['error_level'] = 2;
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
```

These days i just look in the error log but it never hurts to have errors blared at your face.


The most important thing about this code is that i stole it.

I looked at how another module did it.

Implementing hooks is simply following patterns, it would be crazy not to copy.


## Best practices

* Make dependenices optional when possible.
* Provide granular suggestions in theme functions so people can override when they want to.





Mauricio: http://lb.cm/mEP

Altering, Extending, and Enhancing, Drupal 8
Presentation that describes the various ways that Drupal module developers will be able to interact with Drupal 8 core to alter existing functionality, extend Drupal, and enhance it with new functionality. As well as how the various design patterns work in-case they might be useful to implement in your own projects.
Mauricio: YouTube - DrupalCon New Orleans 2016: Altering, Extending, and Enhancing Drupal 8


Mauricio: https://gitlab.com/find-it-program-locator/findit_library_sync/-/blob/8.x-1.x/migrations/findit_library_sync_events.yml#L5

migrations/findit_library_sync_events.yml · 8.x-1.x · Find It Program Locator / Find It Library Sync
A currently fairly hard-coded module (for importing events from Cambridge public libraries) that may evolve into a general import events and programs functionality for the Find It platform.
Mauricio: 1) Specify migration tag ^

Mauricio: 2) Define service https://gitlab.com/find-it-program-locator/findit_library_sync/-/blob/8.x-1.x/findit_library_sync.services.yml

findit_library_sync.services.yml · 8.x-1.x · Find It Program Locator / Find It Library Sync
A currently fairly hard-coded module (for importing events from Cambridge public libraries) that may evolve into a general import events and programs functionality for the Find It platform.
Mauricio: 3) Register event https://gitlab.com/find-it-program-locator/findit_library_sync/-/blob/8.x-1.x/src/EventSubscriber/FindItLibrarySyncSubscriber.php#L73

src/EventSubscriber/FindItLibrarySyncSubscriber.php · 8.x-1.x · Find It Program Locator / Find It Library Sync
A currently fairly hard-coded module (for importing events from Cambridge public libraries) that may evolve into a general import events and programs functionality for the Find It platform.
Mauricio: 4) Execute code https://gitlab.com/find-it-program-locator/findit_library_sync/-/blob/8.x-1.x/src/EventSubscriber/FindItLibrarySyncSubscriber.php#L25

src/EventSubscriber/FindItLibrarySyncSubscriber.php · 8.x-1.x · Find It Program Locator / Find It Library Sync
A currently fairly hard-coded module (for importing events from Cambridge public libraries) that may evolve into a general import events and programs functionality for the Find It platform.
Mauricio: Which is injecting credentials from settings.php

Mauricio: @mlncn I did not find the hook_node_insert example. Something similar is this https://github.com/terravoz/findit/blob/master/web/sites/all/modules/custom/findit_auto_service_provider/findit_auto_service_provider.module

terravoz/findit
Drupal framework for Find It Cambridge. Contribute to terravoz/findit development by creating an account on GitHub.
Mauricio: In the end, the hook_node_insert would be, check if $field_1 is set. It not, then unset($field_2)

Mauricio: I think the service implementation example for the library migration would add more value. Since it has more pieces, it will take more time to explain. So I would not try to fit the node_insert example for now

Mauricio: @mlncn 3 examples should suffice. 1) form alter 2) block plugin 3) event subscriber (find it library migration one)
